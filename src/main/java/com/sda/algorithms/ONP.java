package com.sda.algorithms;

import java.util.Stack;

public class ONP {
    public static void main(String[] args) {
        String onp = convertToONP("( 8 / 2 ) + ( 6 * 7 - 4 ) / 4");
        System.out.println(onp);
        System.out.println(calculateONP(onp));
    }

    public static double calculateONP(String rownanie) {
        String[] onp = rownanie.split(" ");

        Stack<String> stack = new Stack<>();

        for (int i = 0; i < onp.length; i++) {
            String token = onp[i];
            if (!isOperator(token)) {
                stack.push(token);
            } else {
                String val2 = stack.pop();
                String val1 = stack.pop();
                Double result = calculate(token, Double.parseDouble(val1), Double.parseDouble(val2));
                stack.push(String.valueOf(result));
            }
        }
        // wynik będzie na szczycie stosu (na końcu)
        return Double.parseDouble(stack.pop());
    }

    private static Double calculate(String token, double v, double v1) {
        if (token.equals("+")) {
            return v + v1;
        } else if (token.equals("-")) {
            return v - v1;
        } else if (token.equals("/")) {
            return v / v1;
        } else if (token.equals("*")) {
            return v * v1;
        }
        return 0.0;
    }

    public static String convertToONP(String rownanie) {
        String[] expression = rownanie.split(" ");

        Stack<String> stack = new Stack<>();
        StringBuilder onp = new StringBuilder();

        // while expression != none
        for (int i = 0; i < expression.length; i++) {
            // expression, token = get_next_token(expression)
            String token = expression[i];

            // if(is_operand(token)):
            if (!isOperator(token)) { // jest liczba
                // onp = Put(onp, token)
                onp.append(token).append(" ");
            } else if (token.equals("(")) {
                // stack = Push(stack, token)
                stack.push(token);
            } else if (token.equals(")")) {
                // while stack != None :
                while (!stack.isEmpty()) {
                    if (stack.peek().equals("(")) {
                        // if token == "(" : break
                        stack.pop(); // musimy ściągnąć wartość ze stosu
                        break;
                    } else {
                        // else : onp = Put(onp, token)
                        onp.append(stack.pop()).append(" ");
                    }
                }
            } else {
                // priority = get_priority(token)
                int priority = getPriority(token);
                // while stack != None :
                while (!stack.isEmpty()) {
                    // stack,top = Pop(stack)
                    String top = stack.pop();
                    // if top=="(" or get_priority(top)<priority :
                    if (top.equals("(") || getPriority(top) < priority) {
                        // stack = Push(stack,top)
                        stack.push(top);
                        break;
                    }
                    // onp = Put(onp, top)
                    onp.append(top).append(" ");
                }
                // stack = Push(stack,token)
                stack.push(token);
            }
        }
        while (!stack.isEmpty()) {
            onp.append(stack.pop()).append(" ");
        }

        return onp.toString();
    }

    private static int getPriority(String token) {
        if (token.equals("*")) {
            return 2;
        } else if (token.equals("/")) {
            return 2;
        }
        return 1;
    }

    private static boolean isOperator(String token) {
        if (token.equals("+")) {
            return true;
        } else if (token.equals("-")) {
            return true;
        } else if (token.equals("*")) {
            return true;
        } else if (token.equals("/")) {
            return true;
        } else if (token.equals(")")) {
            return true;
        } else if (token.equals("(")) {
            return true;
        }
        return false;
    }
}
